# JPO IPSSI

## Description

Ce projet web permet aux visiteurs des Journées Portes Ouvertes (JPO) de l'IPSSI de laisser leurs informations personnelles via un formulaire en ligne. Les informations sont ensuite stockées dans des fichiers CSV. Une interface permet de télécharger ces fichiers et de les afficher.

## Fonctionnalités

- Formulaire de collecte d'informations personnelles des visiteurs.
- Génération de fichiers CSV pour chaque JPO avec la date de l'événement.
- Interface pour lister et afficher le contenu des fichiers CSV.
- Déploiement automatique via Docker.
- Tests unitaires pour valider les entrées du formulaire.
- Gestion des logs pour le déploiement.

## Installation
Tout d'abord, dans un terminal, exécutez la commande suivante afin d'envoyer les fichiers du projet dans votre VM :
`scp -P 1111 -r chemin/de/votre/dossier/projet-devops/* user@localhost:/home/user/`

Une fois dans la VM, exécutez la commande `ls` pour vérifier que les fichiers sont bien dans votre VM.
Ensuite, exécutez la commande :`docker-compose up`.

Il faudra ensuite configurer votre logiciel de VM afin de rediriger les ports entre l'hôte et votre ordinateur :

| **NOM**      | **PROTOCOLE** | **PORT HÔTE** | **PORT INVITÉ** |
|--------------|---------------|---------------|-----------------|
| VM           | TCP           | 1111          | 22              |
| Formulaire   | TCP           | 5000          | 80              |
| Serveur      | TCP           | 8001          | 8000            |

Allez à l'URL : http://localhost:5000 afin d'avoir accès au formulaire sur votre ordinateur.

### Formulaire de collecte

Accédez à l'URL de votre serveur pour remplir le formulaire JPO et soumettre les informations des visiteurs.

### Affichage des fichiers CSV

Cliquez sur le nom d'un fichier dans la liste pour afficher son contenu.

## Structure des fichiers

- **frontend/** : Contient le code HTML/CSS/JS pour la partie frontend.
- **backend/** : Contient le code PHP pour la partie
