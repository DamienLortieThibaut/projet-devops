document
  .getElementById("registrationForm")
  .addEventListener("submit", async function (e) {
    e.preventDefault();

    const nom = document.getElementById("nom").value;
    const prenom = document.getElementById("prenom").value;
    const telephone = document.getElementById("telephone").value;
    const email = document.getElementById("email").value;
    const formation = document.getElementById("formation").value;
    const accepte = document.getElementById("accepte").checked;

    const options1 = Array.from(
      document.querySelectorAll('input[name="options1[]"]:checked')
    ).map((el) => el.value);
    const options2 = Array.from(
      document.querySelectorAll('input[name="options2[]"]:checked')
    ).map((el) => el.value);

    const date = new Date().toLocaleDateString();

    // Clear previous error messages
    document
      .querySelectorAll(".error-message")
      .forEach((el) => (el.textContent = ""));
    document
      .querySelectorAll(".success-message")
      .forEach((el) => (el.textContent = ""));

    let valid = true;

    // Validation
    if (!nom || /[^a-zA-Z\s]/.test(nom)) {
      document.getElementById("error-nom").textContent =
        "Nom invalide: caractères spéciaux interdits";
      document.getElementById("forms-submit").textContent =
        "Votre formulaire n'est pas correct, merci de le corriger";
      document.getElementById("forms-submit").className = "error-message";
      valid = false;
    }

    if (!prenom || /[^a-zA-Z\s]/.test(prenom)) {
      document.getElementById("error-prenom").textContent =
        "Prénom invalide: caractères spéciaux interdits";
      document.getElementById("forms-submit").textContent =
        "Votre formulaire n'est pas correct, merci de le corriger";
      document.getElementById("forms-submit").className = "error-message";
      valid = false;
    }

    if (!telephone || /[^0-9]/.test(telephone)) {
      document.getElementById("error-telephone").textContent =
        "Téléphone invalide: uniquement des chiffres autorisés";
      document.getElementById("forms-submit").textContent =
        "Votre formulaire n'est pas correct, merci de le corriger";
      document.getElementById("forms-submit").className = "error-message";
      valid = false;
    }

    if (!email || !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      document.getElementById("error-email").textContent = "Email invalide";
      document.getElementById("forms-submit").textContent =
        "Votre formulaire n'est pas correct, merci de le corriger";
      document.getElementById("forms-submit").className = "error-message";
      valid = false;
    }

    if (!formation) {
      document.getElementById("error-formation").textContent =
        "Formation invalide";
      document.getElementById("forms-submit").textContent =
        "Votre formulaire n'est pas correct, merci de le corriger";
      document.getElementById("forms-submit").className = "error-message";
      valid = false;
    }

    if (!accepte) {
      document.getElementById("error-accepte").textContent =
        "Veuillez accepter la conservation et l'utilisation de vos données.";
      document.getElementById("forms-submit").textContent =
        "Votre formulaire n'est pas correct, merci de le corriger";
      document.getElementById("forms-submit").className = "error-message";
      valid = false;
    }

    if (valid) {
      try {
        const newData = {
          nom,
          prenom,
          telephone,
          email,
          formation,
          options1,
          options2,
          date,
        };

        const response = await fetch("http://localhost:8001/saveData.php", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(newData),
        });

        const responseData = await response.json();
        document.getElementById("forms-submit").textContent =
          "Merci de votre participation, votre formulaire a bien été enregistré !";
        document.getElementById("forms-submit").className = "success-message";
        document.getElementById("registrationForm").reset();
      } catch (error) {
        console.error("Erreur lors de la soumission des données:", error);
      }
    }
  });
