<?php
// En-têtes pour autoriser les requêtes CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    http_response_code(200);
    exit();
}

$input = file_get_contents('php://input');
$data = json_decode($input, true);

$directory = __DIR__ . '/csvFiles';

if (!file_exists($directory)) {
    mkdir($directory, 0777, true);
}

$jpoDate = $data['date'];
$fileName = 'PreInscription_' . str_replace('/', '-', $jpoDate) . '.csv';
$filePath = $directory . '/' . $fileName;

$csvFile = fopen($filePath, 'a');

if ($csvFile === false) {
    http_response_code(500);
    echo json_encode(['error' => 'Could not open file for writing']);
    exit();
}

$headers = ['Nom', 'Prénom', 'Téléphone', 'Email', 'Formation', 'interessé par', "j'ai connu l'ipssi grâce", "Date"];
if (filesize($filePath) === 0) {
    fputcsv($csvFile, $headers);
}

$dataRow = [
    $data['nom'],
    $data['prenom'],
    $data['telephone'],
    $data['email'],
    $data['formation'],
    implode('|', $data['options1']),
    implode('|', $data['options2']),
    $data['date'],
];

fputcsv($csvFile, $dataRow);
fclose($csvFile);

http_response_code(200);
echo json_encode(['success' => 'Data saved successfully']);

function validateInput($data) {
    if (empty($data['nom']) || preg_match('/[^a-zA-Z\s]/', $data['nom'])) {
        return false;
    }

    if (empty($data['prenom']) || preg_match('/[^a-zA-Z\s]/', $data['prenom'])) {
        return false;
    }

    if (empty($data['telephone']) || preg_match('/[^0-9]/', $data['telephone'])) {
        return false;
    }

    if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
        return false;
    }

    if (substr_count($data['email'], '@') !== 1 || substr_count($data['email'], '.') < 1) {
        return false;
    }

    if (empty($data['formation'])) {
        return false;
    }

    return true;
}
?>
