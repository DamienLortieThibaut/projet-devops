<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Listings des réponses au questionnaire</title>
  <link rel="stylesheet" href="./styles/styles.css" />
</head>

<body>
  <header>
    <img src="./styles/ipssi-logo.png" alt="Logo" class="logo" />
    <h1>Réponses au formulaire</h1>
  </header>
  <div class="container">
    <h2>Ensemble des réponses</h2>
    <a href="../../frontend/src/index.html">Retour au formulaire</a>
    <ul class="table-csv">
      <?php
      /** Récupérer l'ensemble des csv dans le répertoire approprié */
      $scandir = scandir('./csvFiles');
      foreach ($scandir as $file) {
        /** Vérification que le fichier est bien un .csv */
        if (preg_match("#\.(csv)$#i", $file)) {
          /** Création du listing avec bouton de téléchargement */
          echo "<li>
                          <a href='view.php?file=" . urlencode($file) . "'>" . htmlspecialchars($file) . "</a>
                          <a href='./csvFiles/" . urlencode($file) . "' download>Télécharger</a>
                        </li>";
        }
      }
      ?>
    </ul>
  </div>
</body>

</html>