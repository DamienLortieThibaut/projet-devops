<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./styles/styles.css" />
</head>

<body>
    <header>
        <img src="./styles/ipssi-logo.png" alt="Logo" class="logo">
        <h1>Détail de la journée</h1>
    </header>
    <div class="container">
        <h2>Nom du fichier : <?php echo htmlspecialchars($_GET['file']); ?></h2>
        <a href="./listingCSVFile.php">Retour à la liste</a>
        <?php
        /** Vérifie que le fichier existe */
        if (isset($_GET['file'])) {
            /** Récupération du fichier */
            $file = urldecode($_GET['file']);
            $file_path = './csvFiles/' . $file;
            /** Création du tableau si le fichier existe */
            if (file_exists($file_path)) {
                echo "<table>";
                if (($handle = fopen($file_path, "r")) !== FALSE) {
                    /** Lecture de la première ligne pour les en-têtes */
                    if (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        echo "<thead><tr>";
                        foreach ($data as $header) {
                            /** Affichage des en-têtes de colonne */
                            echo "<th>" . htmlspecialchars($header) . "</th>";
                        }
                        echo "</tr></thead><tbody>";
                    }
                    /** Lecture des lignes suivantes pour les données */
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        echo "<tr>";
                        foreach ($data as $cell) {
                            /** Affichage des cellules */
                            echo "<td>" . htmlspecialchars($cell) . "</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</tbody></table>";
                    /** Fermeture du fichier */
                    fclose($handle);
                } else {
                    /** Message d'erreur si le fichier ne peut pas être ouvert */
                    echo "<p>Impossible d'ouvrir le fichier.</p>";
                }
            } else {
                /** Message d'erreur si le fichier n'existe pas */
                echo "<p>Le fichier n'existe pas.</p>";
            }
        } else {
            /** Message d'erreur si aucun fichier n'a été sélectionné */
            echo "<p>Aucun fichier sélectionné.</p>";
        }
        ?>
    </div>
</body>

</html>