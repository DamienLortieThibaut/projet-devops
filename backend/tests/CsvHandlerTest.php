<?php

use PHPUnit\Framework\TestCase;

class CsvHandlerTest extends TestCase
{
    private $validData;
    private $invalidData;

    protected function setUp(): void
    {
        $this->validData = [
            'nom' => 'Dupont',
            'prenom' => 'Jean',
            'telephone' => '0123456789',
            'email' => 'jean.dupont@example.com',
            'formation' => 'Informatique',
            'options1' => ['Option1'],
            'options2' => ['Option2'],
            'date' => '2024-06-04'
        ];

        $this->invalidData = [
            'nom' => 'Dup@nt',
            'prenom' => 'J$an',
            'telephone' => '01@3456789',
            'email' => 'jeandupontexample.com',
            'formation' => '',
            'options1' => [],
            'options2' => [],
            'date' => '2024-06-04'
        ];
    }

    public function testValidData()
    {
        $this->assertEquals('Valid', $this->validateInput($this->validData));
    }

    public function testInvalidData()
    {
        $this->assertNotEquals('Valid', $this->validateInput($this->invalidData));
    }

    public function testInvalidNom()
    {
        $data = $this->validData;
        $data['nom'] = 'Dupont';
        $this->assertEquals('Nom invalide: caractères spéciaux interdits', $this->validateInput($data));
    }

    public function testInvalidPrenom()
    {
        $data = $this->validData;
        $data['prenom'] = 'Jan';
        $this->assertEquals('Prénom invalide: caractères spéciaux interdits', $this->validateInput($data));
    }

    public function testInvalidTelephone()
    {
        $data = $this->validData;
        $data['telephone'] = '013456789';
        $this->assertEquals('Téléphone invalide: uniquement des chiffres autorisés', $this->validateInput($data));
    }

    public function testInvalidEmail()
    {
        $data = $this->validData;
        $data['email'] = 'jeandupont@example.com';
        $this->assertEquals('Email invalide', $this->validateInput($data));
    }

    public function testEmptyFormation()
    {
        $data = $this->validData;
        $data['formation'] = 'Informatique';
        $this->assertEquals('Formation invalide', $this->validateInput($data));
    }

    private function validateInput($data)
    {
        if (empty($data['nom']) || preg_match('/[^a-zA-Z\s]/', $data['nom'])) {
            return false;
        }

        if (empty($data['prenom']) || preg_match('/[^a-zA-Z\s]/', $data['prenom'])) {
            return false;
        }

        if (empty($data['telephone']) || preg_match('/[^0-9]/', $data['telephone'])) {
            return false;
        }

        if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (substr_count($data['email'], '@') !== 1 || substr_count($data['email'], '.') < 1) {
            return false;
        }

        if (empty($data['formation'])) {
            return false;
        }

        return true;
    }
}
