#!/bin/bash

# Démarrer Apache en arrière-plan
apache2-foreground &

cd /var/www/html/src/

# Démarrer le serveur PHP intégré
php -S 0.0.0.0:8001